CONTENTS OF THIS FILE
---------------------
  
* Introduction
This module contains the Single Page App (SPA), SPA config, and SPA extras
modules for deploying upstream single page applications to your Drupal
environment (http://en.wikipedia.org/wiki/Single-page_application).

* Requirements
The module dependencies for SPA and SPA config are:
- Features
- CTools
- Field group

For SPA extras, the git package is required to deploy git repos to your Drupal
environment.

* Installation
Simply install the module through the UI at admin/modules/, or via drush using
'drush pm-install single_page_app'

* Configuration
If you would like to use the chroming feature (sandboxing the single page
application), you will need to provide a "chrome service" in which Drupal can
access header/footer information to download.

* Administration
Any user with the "Approve Single Page App Git Deployments" can approve
individual SPA content that partake of the git deployment method.

* Troubleshooting
Please file an issue under the project's "Issues" page.

* FAQ
Q: Why is this necessary?
A: The purpose of this module is to provide an easy method for adding and
administrating single page applications in an already existing Drupal
environment.  Many developers do not learn Drupal, but can be very talented in
creating single page applications, and we'd still like to install their content
within our environments.

* Maintainers
Ben Pritchett (bjpritch@redhat.com)
