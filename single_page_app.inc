<?php

/**
 * @file
 * The helper class for interacting with SPA fields and logic.
 */

/**
 * The SinglePageApp helper class.
 *
 * Used to interact with SPA fields and logic from
 * Drupal hooks in main module.
 */
class SinglePageApp {

  /**
   * Fetch the drupal environment's chrome html.
   *
   * @param string $type
   *   The type of chrome to fetch.
   * @param string $lang
   *   The language code for which chrome to pull.
   * @param string $query
   *   Any additional query param to attach.
   *
   * @return string
   *   Cached data.
   */
  public static function spaFetchChrome($type, $lang = 'en', $query = '') {
    $old_cid = '';
    if (!empty($query) && is_array($query)) {
      $query = http_build_query($query);
    }
    if (strlen($chrome_prefix = variable_get('single_page_app_chrome_prefix')) > 0) {
      $chrome_url = $chrome_prefix . $type . '/' . $lang;
      $cid = 'chrome_prefix' . $type . '/' . $lang;
      if (strlen($query) > 0) {
        $chrome_url .= '?' . $query;
        $cid .= '?' . $query;
      }
      $old_cid = 'old/' . $cid;
      if ($cache = cache_get($cid)) {
        if (isset($cache->data) && (strlen($cache->data) > 0)) {
          return $cache->data;
        }
      }
      $http_object = drupal_http_request($chrome_url);

      if ($http_object->code == 200) {
        $response = $http_object->data;
        if (strlen($response) > 0) {
          cache_set($cid, $response, 'cache', time() + (2 * 60 * 60));
          cache_set($old_cid, $response, 'cache', CACHE_PERMANENT);
          return $response;
        }
        else {
          watchdog('single_page_app', "Failed to get chrome service (blank response).", array(), WATCHDOG_WARNING);
        }
      }
      else {
        watchdog('single_page_app', "Failed to get chrome service (Header: %header)", array('%header' => $http_object->code), WATCHDOG_WARNING);
      }
    }
    elseif (variable_get('single_page_app_tpl') != FALSE) {
      return file_get_contents(drupal_get_path('module', 'single_page_app') . '/drupal-' . $type . '.tpl.php');
    }
    return cache_get($old_cid)->data;
  }


  /**
   * Returns the chromed index.html (or other base HTML) for a single page app.
   */
  public static function spaGetIndexHtml($node, $variables = array()) {
    $app_name = self::spaGetPathName($node);
    $realpath = self::getPublicPath();
    $build_path = self::spaGetBuildPath($node);
    $index_html = @file_get_contents($realpath . '/spas/' . $app_name . '/' . $build_path . 'index.html');
    if ($index_html === FALSE) {
      return FALSE;
    }
    $lang = entity_language('node', $node);
    // Putting legacy = false in order to ensure proper usage with chroming.
    $query = array('legacy' => 'false');

    $head = self::spaFetchChrome('head', $lang, $query);
    $header = self::spaFetchChrome('header', $lang, $query);

    $footer = self::spaFetchChrome('footer', $lang, $query);
    $index_html = str_ireplace('<!-- SPA_HEAD -->', $head, $index_html);
    $index_html = str_ireplace('<!-- SPA_HEADER -->', $header, $index_html);
    $index_html = str_ireplace('<!-- SPA_FOOTER -->', $footer, $index_html);
    return $index_html;
  }

  /**
   * Grab the path under spas/ that this app exists at.
   *
   * @param object $node
   *    The node object.
   *
   * @return string
   *    The SPA's path.
   */
  public static function spaGetPathName($node) {
    if (!is_object($node) || $node->type != 'single_page_app') {
      return FALSE;
    }
    $field = field_get_items('node', $node, 'field_spa_install_path_str', LANGUAGE_NONE);
    $output = field_view_value('node', $node, 'field_spa_install_path_str', $field[0]);
    $path_name = $output['#markup'];
    return $path_name;
  }

  /**
   * Helper for getting the git repo.
   *
   * @param object $node
   *    The node object.
   *
   * @return string
   *    The URL of the SPA git repo.
   */
  public static function spaGetGitRepo($node) {
    if (!is_object($node) || $node->type != 'single_page_app') {
      return FALSE;
    }
    $field = field_get_items('node', $node, 'field_spa_git_repo_str', LANGUAGE_NONE);
    $output = field_view_value('node', $node, 'field_spa_git_repo_str', $field[0]);
    $git_repo = $output['#markup'];
    return $git_repo;
  }

  /**
   * Helper function to get External Download URL.
   *
   * @param object $node
   *    The node object.
   *
   * @return bool|string
   *    The URL for where to download a tarball.
   */
  public static function spaGetUrlDownload($node) {
    if (!is_object($node) || $node->type != 'single_page_app') {
      return FALSE;
    }
    $field = field_get_items('node', $node, 'field_spa_install_url_str', LANGUAGE_NONE);
    $output = field_view_value('node', $node, 'field_spa_install_url_str', $field[0]);
    $git_repo = $output['#markup'];
    return $git_repo;
  }

  /**
   * Helper function to get upload location.
   *
   * @param object $node
   *    The node object.
   *
   * @return bool|string
   *    The location on disk of where the SPA tarball is.
   */
  public static function spaGetFileUploadLocation($node) {
    if (!is_object($node) || $node->type != 'single_page_app') {
      return FALSE;
    }
    $field = field_get_items('node', $node, 'field_spa_install_file_file', LANGUAGE_NONE);
    $theme_output = field_view_value('node', $node, 'field_spa_install_file_file', $field[0]);
    $file_object = file_load($theme_output['#file']->fid);
    $public_path = self::getPublicPath();
    return $public_path . '/' . $file_object->filename;
  }

  /**
   * Helper function to get git branch.
   *
   * @param object $node
   *    The node object.
   *
   * @return bool|string
   *    The git branch to deploy.
   */
  public static function spaGetGitBranch($node) {
    if (!is_object($node) || $node->type != 'single_page_app') {
      return FALSE;
    }
    $field = field_get_items('node', $node, 'field_spa_branch_name_str', LANGUAGE_NONE);
    $output = field_view_value('node', $node, 'field_spa_branch_name_str', $field[0]);
    $git_repo = $output['#markup'];
    return $git_repo;
  }

  /**
   * Helper function to get build path.
   *
   * @param object $node
   *    The node object.
   *
   * @return bool|string
   *    The SPA's build path.
   */
  public static function spaGetBuildPath($node) {
    if (!is_object($node) || $node->type != 'single_page_app') {
      return FALSE;
    }
    $field = field_get_items('node', $node, 'field_spa_build_path_str', LANGUAGE_NONE);
    if (!isset($field)) {
      return FALSE;
    }
    $output = field_view_value('node', $node, 'field_spa_build_path_str', $field[0]);
    $build_path = $output['#markup'];
    return $build_path;
  }

  /**
   * Helper function to get all available SPAs.
   *
   * @return mixed
   *    An array of all SPA aliases.
   */
  public static function spaGetAllInstallPaths() {
    $registered_install_paths = db_select('field_data_field_spa_install_path_str', 'i')
      ->fields('i', array('field_spa_install_path_str_value', 'entity_id'))
      ->execute()
      ->fetchAllKeyed();
    return $registered_install_paths;
  }

  /**
   * Puts the SPA in its temporary place on the filesystem.
   *
   * @param object $node
   *    The node object.
   *
   * @return bool
   *    True on success, False on failure.
   */
  public static function spaUrlDownload($node) {
    $archive_location = '';

    if (!file_exists($archive_location)) {
      $archive_location = self::spaGetFileUploadLocation($node);
      if (!file_exists($archive_location)) {
        return FALSE;
      }
    }
    self::spaExtractArchive($node, $archive_location);
    return TRUE;
  }

  /**
   * Puts the SPA in its permanent place on the filesystem.
   *
   * @param object $node
   *    The node object.
   * @param string $current_path
   *    The temporary location the SPA is stored at.
   */
  public static function spaExtractArchive($node, $current_path) {
    $tmp_path = file_directory_temp();
    $public_path = self::getPublicPath();
    if (stripos($current_path, '.zip')) {
      $archive = new ArchiverZip($current_path);
    }
    else {
      $archive = new ArchiverTar($current_path);
    }
    $tmp_clone = $tmp_path . '/' . self::spaGetPathName($node) . '-' . time();
    $archive->extract($tmp_clone);
    if (file_exists($public_path . '/spas/' . self::spaGetPathName($node))) {
      $new_md5 = self::md5dir($tmp_clone);
      $old_md5 = self::md5dir($public_path . '/spas/' . self::spaGetPathName($node));
      if ($new_md5 != $old_md5) {
        $copy_status = self::copyDirectory($tmp_clone, $public_path . '/spas/' . self::spaGetPathName($node));
        if ($copy_status === FALSE) {
          watchdog('single_page_app', 'An error occurred copying ' . $node->title . ' [nid: ' . $node->nid . '] to its final location.', array(), WATCHDOG_WARNING);
        }
      }
    }
    else {
      $copy_status = self::copyDirectory($tmp_clone, $public_path . '/spas/' . self::spaGetPathName($node));
      if ($copy_status === FALSE) {
        watchdog('single_page_app', 'An error occurred copying ' . $node->title . ' [nid: ' . $node->nid . '] to its final location.', array(), WATCHDOG_WARNING);
      }
    }
    file_unmanaged_delete_recursive($tmp_clone);
  }

  /**
   * Grabs a list of all installed SPAs' aliases for comparison.
   *
   * @return array
   *    The list of current SPA aliases.
   */
  public static function spaGetUrlAliases() {
    $aliases = array();
    $result = db_query('SELECT n.nid from {node} n where type = :type', array(':type' => "single_page_app"));
    foreach ($result as $record) {
      $alias = db_query('SELECT u.alias from {url_alias} u where source = :source', array(':source' => 'node/' . $record->nid));
      $alias = $alias->fetchObject();
      if (isset($alias->alias)) {
        $aliases[$record->nid] = $alias->alias;
      }
    }
    return ($aliases);
  }

  /**
   * Helper to get Drupal public path.
   *
   * @return bool
   *    Drupal's public path on the filesystem.
   */
  public static function getPublicPath() {
    if ($wrapper = file_stream_wrapper_get_instance_by_uri('public://')) {
      return $wrapper->realpath();
    }
    return FALSE;
  }

  /**
   * Borrowed and modified from PHP.net.
   */
  public static function md5dir($dir) {
    if (!is_dir($dir)) {
      return FALSE;
    }
    $filemd5s = array();
    $handle = opendir($dir);

    while (FALSE !== ($entry = readdir($handle))) {
      if ($entry != '.' && $entry != '..') {
        if (is_dir($dir . '/' . $entry)) {
          $filemd5s[] = self::md5dir($dir . '/' . $entry);
        }
        else {
          $filemd5s[] = md5_file($dir . '/' . $entry);
        }
      }
    }
    closedir($handle);
    return md5(implode('', $filemd5s));
  }

  /**
   * Borrowed and modified from PHP.net.
   */
  public static function copyDirectory($src, $dst) {
    $dir = opendir($src);
    $status = TRUE;
    @drupal_mkdir($dst);
    while (FALSE !== ($file = readdir($dir))) {
      if (($file != '.') && ($file != '..')) {
        if (is_dir($src . '/' . $file)) {
          $status = self::copyDirectory($src . '/' . $file, $dst . '/' . $file);
        }
        else {
          $status = file_unmanaged_copy($src . '/' . $file, $dst . '/' . $file, FILE_EXISTS_REPLACE);
        }
      }
    }
    closedir($dir);
    return $status;
  }

}
