  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
  </div>
    <div class="region region-page-top">
    <div id="toolbar" class="toolbar overlay-displace-top clearfix">
  <div class="toolbar-menu clearfix">
    <ul id="toolbar-home"><li class="home first last active"><a href="/drupal/" title="Home" class="active"><span class="home-link">Home</span></a></li>
</ul>    <ul id="toolbar-user"><li class="account first"><a href="/drupal/user" title="User account">Hello <strong>admin</strong></a></li>
<li class="logout last"><a href="/drupal/user/logout">Log out</a></li>
</ul>    <h2 class="element-invisible">Administrative toolbar</h2><ul id="toolbar-menu"><li class="menu-11 path-admin-dashboard first"><a href="/drupal/admin/dashboard" id="toolbar-link-admin-dashboard" title="View and customize your dashboard."><span class="icon"></span>Dashboard <span class="element-invisible">(View and customize your dashboard.)</span></a></li>
<li class="menu-9 path-admin-content"><a href="/drupal/admin/content" id="toolbar-link-admin-content" title="Administer content and comments."><span class="icon"></span>Content <span class="element-invisible">(Administer content and comments.)</span></a></li>
<li class="menu-21 path-admin-structure"><a href="/drupal/admin/structure" id="toolbar-link-admin-structure" title="Administer blocks, content types, menus, etc."><span class="icon"></span>Structure <span class="element-invisible">(Administer blocks, content types, menus, etc.)</span></a></li>
<li class="menu-7 path-admin-appearance"><a href="/drupal/admin/appearance" id="toolbar-link-admin-appearance" title="Select and configure your themes."><span class="icon"></span>Appearance <span class="element-invisible">(Select and configure your themes.)</span></a></li>
<li class="menu-18 path-admin-people"><a href="/drupal/admin/people" id="toolbar-link-admin-people" title="Manage user accounts, roles, and permissions."><span class="icon"></span>People <span class="element-invisible">(Manage user accounts, roles, and permissions.)</span></a></li>
<li class="menu-16 path-admin-modules"><a href="/drupal/admin/modules" id="toolbar-link-admin-modules" title="Extend site functionality."><span class="icon"></span>Modules <span class="element-invisible">(Extend site functionality.)</span></a></li>
<li class="menu-8 path-admin-config"><a href="/drupal/admin/config" id="toolbar-link-admin-config" title="Administer settings."><span class="icon"></span>Configuration <span class="element-invisible">(Administer settings.)</span></a></li>
<li class="menu-19 path-admin-reports"><a href="/drupal/admin/reports" id="toolbar-link-admin-reports" title="View reports, updates, and errors."><span class="icon"></span>Reports <span class="element-invisible">(View reports, updates, and errors.)</span></a></li>
<li class="menu-12 path-admin-help last"><a href="/drupal/admin/help" id="toolbar-link-admin-help" title="Reference for usage, configuration, and modules."><span class="icon"></span>Help <span class="element-invisible">(Reference for usage, configuration, and modules.)</span></a></li>
</ul>          <a href="/drupal/toolbar/toggle?destination=node" title="Hide shortcuts" class="toggle toggle-active">Hide shortcuts</a>      </div>

  <div class="toolbar-drawer clearfix">
    <div class="toolbar-shortcuts"><ul class="menu clearfix"><li class="first leaf"><a href="/drupal/node/add">Add content</a></li>
<li class="leaf"><a href="/drupal/admin/content">Find content</a></li>
<li class="last leaf"><a href="/drupal/admin/config/development/performance">Performance</a></li>
</ul></div><a href="/drupal/admin/config/user-interface/shortcut/shortcut-set-1" id="edit-shortcuts">Edit shortcuts</a>  </div>
</div>
  </div>
  <div id="page-wrapper"><div id="page">

  <div id="header" class="with-secondary-menu"><div class="section clearfix">

          <a href="/drupal/" title="Home" rel="home" id="logo">
        <img src="http://localhost/drupal/themes/bartik/logo.png" alt="Home" />
      </a>
    
          <div id="name-and-slogan">

                              <div id="site-name">
              <strong>
                <a href="/drupal/" title="Home" rel="home"><span>localhost</span></a>
              </strong>
            </div>
                  
        
      </div> <!-- /#name-and-slogan -->
    
    
          <div id="main-menu" class="navigation">
        <h2 class="element-invisible">Main menu</h2><ul id="main-menu-links" class="links clearfix"><li class="menu-218 first last active"><a href="/drupal/" class="active">Home</a></li>
</ul>      </div> <!-- /#main-menu -->
    
          <div id="secondary-menu" class="navigation">
        <h2 class="element-invisible">Secondary menu</h2><ul id="secondary-menu-links" class="links inline clearfix"><li class="menu-2 first"><a href="/drupal/user">My account</a></li>
<li class="menu-15 last"><a href="/drupal/user/logout">Log out</a></li>
</ul>      </div> <!-- /#secondary-menu -->
    
  </div></div> <!-- /.section, /#header -->
