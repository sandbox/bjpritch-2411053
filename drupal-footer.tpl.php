<div id="footer-wrapper"><div class="section">

    
          <div id="footer" class="clearfix">
          <div class="region region-footer">
    <div id="block-system-powered-by" class="block block-system contextual-links-region">

    <div class="contextual-links-wrapper"><ul class="contextual-links"><li class="block-configure first last"><a href="/drupal/admin/structure/block/manage/system/powered-by/configure?destination=node">Configure block</a></li>
</ul></div>
  <div class="content">
    <span>Powered by <a href="http://drupal.org">Drupal</a></span>  </div>
</div>
  </div>
      </div> <!-- /#footer -->
    
  </div></div> <!-- /.section, /#footer-wrapper -->

</div></div> <!-- /#page, /#page-wrapper -->
