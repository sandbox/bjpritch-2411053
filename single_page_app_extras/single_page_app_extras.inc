<?php

/**
 * @file
 * Helper functions for the SPA Extras module.
 */

/**
 * The SinglePageAppExtras helper class.
 *
 * Used to interact with the Drupal environment and DB.
 */
class SinglePageAppExtras extends SinglePageApp {

  /**
   * Handles the clone of an approved git repo.
   *
   * @param object $node
   *    The $node object for this SPA.
   *
   * @return bool
   *    Whether the clone was successful or not
   */
  public static function spaExtrasGitDownload($node) {
    $tmp_path = file_directory_temp();
    $tmp_clone = $tmp_path . '/' . self::spaGetPathName($node) . '-' . time();
    $repo = self::spaGetGitRepo($node);
    $git_clone = 'git clone --recursive';
    $branch = 'master';

    if (strlen($new_branch = self::spaGetGitBranch($node)) > 0) {
      $branch = $new_branch;
    }

    $public_path = self::getPublicPath();

    exec($git_clone . ' --branch ' . escapeshellarg($branch) . ' ' . escapeshellarg($repo) . ' ' . escapeshellarg($tmp_clone), $output, $status);
    if (file_exists($public_path . '/spas/' . self::spaGetPathName($node))) {
      $new_md5 = self::md5dir($tmp_path . '/' . self::spaGetPathName($node));
      $old_md5 = self::md5dir($public_path . '/spas/' . self::spaGetPathName($node));
      if ($new_md5 != $old_md5) {
        $copy_status = self::copyDirectory($tmp_clone, $public_path . '/spas/' . self::spaGetPathName($node));
        if ($copy_status === FALSE) {
          watchdog('single_page_app', 'An error occurred copying ' . $node->title . ' [nid: ' . $node->nid . '] to its final location.', array(), WATCHDOG_WARNING);
        }
        else {
          watchdog('single_page_app', 'Successfully copied ' . $node->title . ' [nid: ' . $node->nid . '] to its final location.', array(), WATCHDOG_INFO);
        }
      }
    }
    else {
      $copy_status = self::copyDirectory($tmp_clone, $public_path . '/spas/' . self::spaGetPathName($node));
      if ($copy_status === FALSE) {
        watchdog('single_page_app', 'An error occurred copying ' . $node->title . ' [nid: ' . $node->nid . '] to its final location.', array(), WATCHDOG_WARNING);
      }
      else {
        watchdog('single_page_app', 'Successfully copied ' . $node->title . ' [nid: ' . $node->nid . '] to its final location.', array(), WATCHDOG_INFO);
      }
    }
    file_unmanaged_delete_recursive($tmp_clone);
    return $status;
  }

  /**
   * Whether the git repo has been approved.
   *
   * @param object $node
   *    The node object.
   *
   * @return bool
   *    Approved or not.
   */
  public static function spaExtrasIsGitApproved($node) {
    $proposed_git_repo = $node->field_spa_git_repo_str[LANGUAGE_NONE][0]['value'];
    $queried_git_repo = db_select('spa_extras_approval', 'sea')
      ->fields('sea')
      ->condition('repo', $proposed_git_repo, '=')
      ->execute()
      ->fetchAssoc();
    if (isset($queried_git_repo['repo']) && $queried_git_repo['status'] == 1) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Simple function to check whether this repo exists.
   *
   * @param string $repo
   *    The git repo URL.
   *
   * @return bool
   *    Successful ping (TRUE) or not (FALSE).
   */
  public static function spaExtrasGitPing($repo) {
    $git_ls = 'git ls-remote';
    $output = array();
    $status = 128;
    exec($git_ls . ' ' . escapeshellarg($repo), $output, $status);
    if ($status !== 0) {
      return FALSE;
    }
    return TRUE;
  }

}
