<?php
/**
 * @file
 * Single_page_app_config.features.inc.
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function single_page_app_config_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function single_page_app_config_node_info() {
  $items = array(
    'single_page_app' => array(
      'name' => t('Single Page App'),
      'base' => 'node_content',
      'description' => t('The Single Page App content type allows users to create and manage applications existing on one page in Drupal.'),
      'has_title' => '1',
      'title_label' => t('App Name'),
      'help' => '',
    ),
  );
  return $items;
}
